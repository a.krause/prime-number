import java.io.BufferedReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ReadFile {
    public List <Long> downloadNumbers() throws URISyntaxException {
        final String title="numbers.txt";
        URI uri= getClass().getClassLoader().getResource(title).toURI();
        Path filePath= Paths.get(uri);
        Charset charset=Charset.forName("UTF-8");

        List<Long> numbers=new ArrayList<>();
        try  (BufferedReader bufferedReader= Files.newBufferedReader(filePath,charset)){
        String line;
        while ((line=bufferedReader.readLine())!=null){
            numbers.add(Long.valueOf(line));

        }




        } catch (IOException e) {
            e.printStackTrace();
        }
        return numbers;


    }

}
// a.krause