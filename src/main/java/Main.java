import java.net.URISyntaxException;
import java.util.List;

public class Main {
    public static void main(String[] args)throws URISyntaxException,InterruptedException {
        long start,end,time;
        start=System.currentTimeMillis();
        ReadFile readFile=new ReadFile();
        PrimeNumber primeNumber=new PrimeNumber();
        List<Long>numbers =readFile.downloadNumbers();
        int howManyNumbers=primeNumber.fromFile(numbers);
        end=System.currentTimeMillis();
        System.out.println(howManyNumbers);
        time=end-start;
        System.out.println("time"+"" +time/1000+"sec");

    }
}
// a.krause