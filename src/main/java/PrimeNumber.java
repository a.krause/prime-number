import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Math.sqrt;

public class PrimeNumber {
    public  static AtomicInteger numb = new AtomicInteger();

    private ExecutorService  tredPool = Executors.newFixedThreadPool(4);
    private CountDownLatch countDownLatch;
    boolean isPrime(long number){
        if (number<=0){
            return false;

        }
        if (number>2 && number%2 ==0){
            return false;
        }
        int top =(int) sqrt(number)+1;
        for (int i=2;i<top;i++){
            if (number% i==0){
                return false;
            }
        }
        return true;
    }

    public int fromFile(List<Long> numbers)throws  InterruptedException{
        countDownLatch=new CountDownLatch(numbers.size());
        for (Long nr:numbers) {
            tredPool.submit(() -> {
                        boolean prime;
                        prime = isPrime(nr);
                        if (prime) {

                            numb.incrementAndGet();

                        }
                        countDownLatch.countDown();
                    });


            }

        countDownLatch.await();
        tredPool.shutdown();
        return numb.get();


    }

    }

// a.krause