import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PrimeNumberTest {
    private PrimeNumber primeNumber=new PrimeNumber();
    @Test
    public void fiveIsPrimeNumber(){
        assertTrue(primeNumber.isPrime(5));

    }
    @Test
    public void sixIsNotPeime(){
        assertFalse(primeNumber.isPrime(6));
    }
    @Test
    public  void  minusOneIsFalse(){
        assertFalse(primeNumber.isPrime(-1));
    }
}
// a.krause